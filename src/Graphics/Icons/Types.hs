module Graphics.Icons.Types where

-- |  The IconCode class is to represent the icon font that a particular glyph belongs to.  The type contains the font and the functions allow the glyph to be translated into various forms.
class IconCode a where
  -- | Displays the icon in the Pango markup rendering engine.
  iconPango :: a -> String
