# Revision history for icon-fonts

## 0.2.2.0 -- 2018-04-02

* Remove strict bounds on base.

This package is essentially a pack on constants.  Short of the next haskell report turning the language into Whitespace or Piet, this could should always be forward compatible.

## 0.2.1.0  -- 2017-02-02

* Add the weather icons

## 0.2.0.0  -- 2017-02-02

* Fix Graphics.Icons.AllTheIcons to have the correct types

The types weren't defined properly, so this is a breaking change as the types have changed.  Not that it really matters, since I haven't even pushed this onto github yet.


## 0.1.0.0  -- 2017-02-02

* First version. Released on an unsuspecting world.
